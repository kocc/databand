# flume 数据采集

flume是一个分布式、可靠、和高可用的海量日志采集、聚合和传输的系统。支持在日志系统中定制各类数据发送方，用于收集数据;同时，Flume提供对数据进行简单处理，并写到各种数据接受方(比如文本、HDFS、Hbase等)的能力 。  
flume的数据流由事件(Event)贯穿始终。事件是Flume的基本数据单位，它携带日志数据(字节数组形式)并且携带有头信息，这些Event由Agent外部的Source生成，当Source捕获事件后会进行特定的格式化，然后Source会把事件推入(单个或多个)Channel中。  
你可以把Channel看作是一个缓冲区，它将保存事件直到Sink处理完该事件。  
Sink负责持久化日志或者把事件推向另一个Source。  
1）flume的可靠性  
　　　　当节点出现故障时，日志能够被传送到其他节点上而不会丢失。Flume提供了三种级别的可靠性保障，从强到弱依次分别为：end-to-end（收到数据agent首先将event写到磁盘上，当数据传送成功后，再删除；如果数据发送失败，可以重新发送。），Store on failure（这也是scribe采用的策略，当数据接收方crash时，将数据写到本地，待恢复后，继续发送），Besteffort（数据发送到接收方后，不会进行确认）。  

2）flume的可恢复性  
　　　　还是靠Channel。推荐使用FileChannel，事件持久化在本地文件系统里(性能较差)。  

#### 完整开发使用文档

https://www.cnblogs.com/starcrm/p/11909979.html

#### 代码

https://gitee.com/475660/databand/tree/master/databand-mock-log

本目录仅存放配置文件。