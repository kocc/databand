package org.databandtech.logmock;

import java.util.Properties;

import org.apache.flume.api.RpcClientConfigurationConstants;
import org.databandtech.logmock.rpc.RpcClientFacade;
import org.databandtech.logmock.rpc.SecureRpcClientFacade;

public class RpcMock {
	
	/*
	 * 测试,从rpc输出到console,运行后可以用flume进行测试，配置文件见：/flumeConf/avro-memory-log.properties：
	 * 运行脚本
	 * flume-ng agent --conf conf --conf-file /usr/app/apache-flume-1.8.0-bin/avro-memory-log.properties --name a2 -Dflume.root.logger=INFO,console
	 */
	static String HOST="192.168.13.52";
	static int PORT=8888;
	
	public static void main( String[] args )
    {	
		rpcSend();   
		//secureRpcSend();
    }

	private static void rpcSend() {
		RpcClientFacade client = new RpcClientFacade();
		client.init(HOST,PORT);
		String sampleData = "Flume data";
		for (int i = 0; i < 10; i++) {
		  client.sendData(sampleData+i);
		}
		client.cleanUp();
	}

	/*
	 * SSL RPC 发送
	 */
	private static void secureRpcSend() {
		SecureRpcClientFacade client = new SecureRpcClientFacade();
		// Initialize client with the remote Flume agent's host, port
		Properties props = new Properties();
		props.setProperty(RpcClientConfigurationConstants.CONFIG_CLIENT_TYPE, "thrift");
		props.setProperty("hosts", "h1");
		props.setProperty("hosts.h1", HOST+":"+ String.valueOf(PORT));

		// Initialize client with the kerberos authentication related properties
		props.setProperty("kerberos", "true");
		props.setProperty("client-principal", "flumeclient/client.example.org@EXAMPLE.ORG");
		props.setProperty("client-keytab", "/tmp/flumeclient.keytab");
		props.setProperty("server-principal", "flume/server.example.org@EXAMPLE.ORG");
		client.init(props);

		// Send 10 events to the remote Flume agent. That agent should be
		// configured to listen with an AvroSource.
		String sampleData = "Hello Flume!";
		for (int i = 0; i < 10; i++) {
		  client.sendData(sampleData);
		}

		client.cleanUp();
	}

}
