package org.databandtech.mysql2redis.util;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisUtil {
	
	private static volatile JedisPool jedisPool = null;
	private static String HOST = "192.168.13.52";
	private static int PORT = 6379;

    public static Jedis getResource() {
        if (null == jedisPool) {
            synchronized (RedisUtil.class) {
                if (null == jedisPool) {
                    JedisPoolConfig config = new JedisPoolConfig();
                    config.setMaxTotal(10);
                    config.setMaxIdle(2);
                    config.setMaxWaitMillis(2000);
                    config.setTestOnBorrow(true);
                    jedisPool = new JedisPool(config,HOST,PORT );
                }
            }
        }
        return jedisPool.getResource();
    }

}
