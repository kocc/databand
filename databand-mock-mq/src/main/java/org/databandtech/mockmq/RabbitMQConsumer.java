package org.databandtech.mockmq;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class RabbitMQConsumer {
	
	static final String USERNAME = "guest";
	static final String PASSWORD = "guest";
	static final String VIRTUALHOST = "/";
	static final String HOSTNAME = "localhost";
	static final String EXCHANGENAME = "my-exchange";
	static final String ROUTINGKEY = "my-key";
	final static String QUEUE_NAME="rabbitMQ_QUEUE";

	public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
		factory.setUsername(USERNAME);
		factory.setPassword(PASSWORD);
		factory.setVirtualHost(VIRTUALHOST);
		factory.setHost(HOSTNAME);
        //建立到代理服务器到连接
        Connection conn = factory.newConnection();
        //获得信道
        final Channel channel = conn.createChannel();
        channel.exchangeDeclare(EXCHANGENAME, "direct", true);
        //声明要关注的队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        //声明队列
        String queueName = channel.queueDeclare().getQueue();
        //绑定队列，通过键 hola 将队列和交换器绑定起来
        channel.queueBind(queueName, EXCHANGENAME, ROUTINGKEY);

        while(true) {
            //消费消息
            boolean autoAck = false;
            String consumerTag = "";
            channel.basicConsume(queueName, autoAck, consumerTag, new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag,
                                           Envelope envelope,
                                           AMQP.BasicProperties properties,
                                           byte[] body) throws IOException {
                    String routingKey = envelope.getRoutingKey();
                    String contentType = properties.getContentType();
                    System.out.println("消费的路由键：" + routingKey);
                    System.out.println("消费的内容类型：" + contentType);
                    long deliveryTag = envelope.getDeliveryTag();
                    //确认消息
                    channel.basicAck(deliveryTag, false);
                    System.out.println("消费的消息体内容：");
                    String bodyStr = new String(body, "UTF-8");
                    System.out.println(bodyStr);

                }
            });
        }
    }

}
